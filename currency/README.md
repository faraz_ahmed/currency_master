# README #

* Maven JSP project.
* Clone. 
* Import as existing maven project
* Add tomcat 8.5 as target runtime
* Set connection information in: masterdata.data.DataAccessor

# Create Tables in MySQL #
```SQL
CREATE TABLE `country` (
  `countryCode` varchar(3) NOT NULL,
  `countryName` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`countryCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `currency` (
  `currencyCode` varchar(3) NOT NULL,
  `currencyDescription` varchar(30) DEFAULT NULL,
  `countryCode` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`currencyCode`),
  KEY `countryCode_idx` (`countryCode`),
  CONSTRAINT `countryCode` FOREIGN KEY (`countryCode`) REFERENCES `country` (`countryCode`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

```

# Add Data #
```SQL
INSERT INTO `country` (`countryCode`, `countryName`) VALUES ('CAN', 'Canada');
INSERT INTO `country` (`countryCode`, `countryName`) VALUES ('USA', 'United State');

INSERT INTO `currency` (`currencyCode`, `currencyDescription`, `countryCode`) VALUES ('CAD', 'Canadian Dollars', 'CAN');
INSERT INTO `currency` (`currencyCode`, `currencyDescription`, `countryCode`) VALUES ('USD', 'US Dollars', 'USA');
```
