<%@page import="java.util.List"%>
<%@page import="masterdata.data.Field"%>
<%@page import="masterdata.data.DataResult"%>
<%@page import="masterdata.data.SortOrder"%>
<%@page import="masterdata.data.DataReader"%>
<%@page import="masterdata.loopups.SearchCaptionLookUp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<%
request.setAttribute("searchTypeCaption", SearchCaptionLookUp.readCaptionForType(request.getParameter("searchType")));
%>

<t:masterpage title="Search/Create">
 <t:search title="${searchTypeCaption }" searchType="${param.searchType}" searchTerm="${param.searchTerm}"></t:search>
 <t:list></t:list>
</t:masterpage>

