<%@tag description="Search Component" pageEncoding="UTF-8"%>
<!-- Can only be used once on a page (for now) -->
<%@attribute name="title" required="true"%>
<%@attribute name="searchType" required="true"%>
<%@attribute name="searchTerm" %>


 <div class="container-fluid">
  <div class="row">
   <div class="col">
    <h3>${title}</h3>
   </div>
  </div>
  <div class="row">
   <div class="col-4">
    <form action="search.jsp" method="get">
     <input type="hidden" value="${searchType}" name="searchType">
     <input type="hidden" value="0" name="startIndex">
     <input type="hidden" value="5" name="length">
     <div class="form-group">
      <label for="searchTerm">Search: </label> 
      <input type="text" class="form-control" id="searchTerm" name="searchTerm" placeholder="Enter search" value="${searchTerm}">
      <div class="alert alert-warning hide no-search-term-alert" style="display: none;" role="alert">
       Please enter search term
      </div>
     </div>
     <button type="submit" class="btn btn-primary search">Submit</button>
     <button class="btn btn-success create">Add New</button>
    </form>
   </div>
  </div>
 </div>
 
  <script>
  $(".search").click(function(e) {
//	  if(!$("#searchTerm").val()) {
//		 $(".no-search-term-alert").show();
//	     e.preventDefault(); 
//	     return false;
//	  }
  });
  
  $(".create").click(function(e) {
	 document.location="create.jsp?mode=insert&objectType=<%=searchType%>";
     e.preventDefault(); 
     return false;
  });  
 </script>