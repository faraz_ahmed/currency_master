<%@tag import="masterdata.data.UpdateResult"%>
<%@tag import="com.google.common.collect.ImmutableMap"%>
<%@tag import="masterdata.data.DataUpdater"%>
<%@tag import="masterdata.data.Filter"%>
<%@tag import="java.util.Collections"%>
<%@tag import="java.util.Map"%>
<%@tag import="java.util.List"%>
<%@tag import="masterdata.data.SortOrder"%>
<%@tag import="masterdata.data.DataResult"%>
<%@tag import="masterdata.data.DataReader"%>
<%

// Mode of the form to load in.
String mode = request.getParameter("mode");

if(!"insert".equals(mode) && !"update".equals(mode) && !"delete".equals(mode)) {
  throw new RuntimeException("mode should be insert or update or delete. ");
}

// Do DB write or not (Depending on mode)
boolean dbWrite = request.getParameter("dbWrite") != null;

String currencyCode = "";
String currencyDescription = "";
String countryCode = "";

boolean hasError = false;
String errorMessage = "";

DataReader reader = new DataReader();

if(dbWrite) {
  // Time to write to DB.
  // All information should be in request

  currencyCode = request.getParameter("currencyCode");
  currencyDescription = request.getParameter("currencyDescription");
  countryCode = request.getParameter("countryCode");
  
  if("insert".equals(mode)) {
    // Perform insert
    UpdateResult result = DataUpdater.insert("currency", "currencyCode", 
        ImmutableMap.of("currencyCode", currencyCode, "currencyDescription", currencyDescription, "countryCode", countryCode));
    hasError = !result.isSuccessful();
    errorMessage = result.getErrorMessage();
    
  } else if("update".equals(mode)) {
    // Perform update
    UpdateResult result = DataUpdater.update("currency", "currencyCode", 
        ImmutableMap.of("currencyCode", currencyCode, "currencyDescription", currencyDescription, "countryCode", countryCode));
    hasError = !result.isSuccessful();
    errorMessage = result.getErrorMessage();
  } else if("delete".equals(mode)) {
    UpdateResult result = DataUpdater.delete("currency", "currencyCode", currencyCode);
    hasError = !result.isSuccessful();
    errorMessage = result.getErrorMessage();
  }
} else {
  // This is not a write back
  if("insert".equals(mode)) {
    // In case of insert wipe clean everything
    currencyCode = "";
    currencyDescription = "";
    countryCode = "";
    
  } else if("update".equals(mode)) {
    // In case of update read the record.
    currencyCode = request.getParameter("currencyCode");
    DataResult currency = reader.read("currency", null, null, -1, -1, null, new Filter("currencyCode", "'" + currencyCode + "'"));
    if(currency.getRows().size() > 0) {
      currencyDescription = currency.getRows().get(0).get(1).toString();
      //countryName = currency.getRows().get(0).get(2).toString();
      countryCode = currency.getRows().get(0).get(3).toString();
    }
  }
}

boolean proceed = true;

if(dbWrite) {
  // If has error the proceed with sowing the page
  proceed = hasError;
}

if(proceed) {

%>
<div class="container-fluid">
 <div class="row">
  <div class="col">
  
   <h3><%="update".equals(mode) ? "Update currency record" : "Create new currency record"%></h3>
  </div>
 </div>
 <div class="row">
  <div class="col-4">
   <form action="create.jsp" method="post">
    <div class="form-group">
     <label for="currencyCode">Code: </label> 
     <input maxlength="3" <%="update".equals(mode)?"readonly": "" %> type="text" class="form-control" id="currencyCode" name="currencyCode" placeholder="Enter code" value="<%=currencyCode%>">
    </div>
    <div class="form-group">
     <label for="currencyDescription">Name: </label> 
     <input maxlength="30" type="text" class="form-control" id="currencyDescription" name="currencyDescription" placeholder="Enter name" value="<%=currencyDescription%>">
    </div>
    <div class="form-group">
     <label for="currencyCode">Country: </label>

     <%
      
      DataResult result = reader.read("country", "countryName", SortOrder.asc, -1, -1, null);
      %>

     <select class="form-control" id="countryCode" name="countryCode">
     <%
      for(List<Object> row : result.getRows()) {
        %><option value="<%=row.get(1)%>" <%=row.get(1).equals(countryCode) ? "selected" : "" %>><%=row.get(0)%></option><%
      }
     %>
     </select> 
    </div>
    <input type="hidden" name="dbWrite" value="1">
    <input type="hidden" name="mode" value="<%=mode%>">
    <% if(hasError) { %>
    <div class="alert alert-danger" role="alert"><%=errorMessage %></div>
    <%} %>
    <button class="btn btn-success create"><%="update".equals(mode) ? "Update" : "Add New"%></button>
   </form>
  </div>
 </div>
</div>
<%} else {
  response.sendRedirect("search.jsp?searchType=currency&startIndex=0&length=5&mode="+ mode +"&searchTerm=&currencyCode=" + currencyCode);
}

%>