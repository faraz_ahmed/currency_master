<!-- The search display with sorting, paging can be made a generic component too -->
<!-- That component can work directly on tables or DB views like:
     SELECT currencyCode, currencyDescription, countryName FROM currency, country where currency.countryCode = country.countryCode; -->
<!-- For now keeping is specific for this assignment -->

<%@tag import="masterdata.data.Filter"%>
<%@tag import="java.util.List"%>
<%@tag import="masterdata.data.Field"%>
<%@tag import="masterdata.data.DataResult"%>
<%@tag import="masterdata.data.SortOrder"%>
<%@tag import="masterdata.data.DataReader"%>
<%
 DataReader reader = new DataReader();
 String sortField = request.getParameter("sortField");   
 SortOrder order = null;
 int startIndex = -1;
 int length = -1;
 String searchTerm = request.getParameter("searchTerm");
 
 try {
   order = SortOrder.valueOf(request.getParameter("order"));
  } catch(Exception e) { }
  
 try {
   startIndex = Integer.valueOf(request.getParameter("startIndex"));
  } catch(Exception e) { }
  
 try {
   length = Integer.valueOf(request.getParameter("length"));
  } catch(Exception e) { }
  
 String searchType = request.getParameter("searchType");
 double totalRecords = reader.count(searchType, searchTerm);
 
 double pageSize = 5;
 
 double totalPages =  Math.ceil(totalRecords / pageSize);
 
 double currentPage = startIndex == -1 ? 1 : startIndex / pageSize;
 
 if(startIndex == -1) {
   startIndex = 0;
   length = (int)pageSize;
 }

 DataResult result = reader.read(searchType, sortField, order, startIndex, length, searchTerm);

%>

<%
String currencyCode = request.getParameter("currencyCode");
String currencyDescription = "";
String countryName = "";
String countryCode = "";
DataResult currency = reader.read("currency", null, null, -1, -1, null, new Filter("currencyCode", "'" + currencyCode + "'"));

boolean recordFound = false;
if(currency.getRows().size() > 0) {
  currencyDescription = currency.getRows().get(0).get(1).toString();
  countryName = currency.getRows().get(0).get(2).toString();
  countryCode = currency.getRows().get(0).get(3).toString();
  recordFound = true;
}

String mode = request.getParameter("mode"); 


%>
<br>
<br>
<div class="container-fluid">
<%if(recordFound) { %>
<div class="alert alert-success" role="alert">
  Currency <%="insert".equals(mode)? "created":"updated" %>: <strong><%=currencyDescription %> (<%=currencyCode %>) - <%=countryName %></strong>
</div>
<%} %>
<div class="row">
 <div class="col-9">
  <h3>Results (Total: <%=(long)totalRecords %>)</h3>
 </div>
 <div class="col-3">
  <nav aria-label="Page navigation example">
  <ul class="pagination">
    <li class="page-item <%=currentPage > 0 ? "" : "disabled" %>" 
    <%if(currentPage > 0) {%>
    onclick="load('<%=searchType%>', '<%=sortField %>', '<%=order%>', '<%=(long)((currentPage-1)*pageSize)%>', '<%=length%>', '<%=searchTerm%>')"
    <%} %>
    ><a class="page-link" href="#">Previous</a></li>

   <%for(double i = 0.0; i < totalPages; i++) {%>
    <li class="page-item <%=currentPage==i ? "active" : "" %>"
     onclick="load('<%=searchType%>', '<%=sortField %>', '<%=order%>', '<%=(long)(i*pageSize)%>', '<%=length%>', '<%=searchTerm%>')"
    ><a class="page-link" href="#"><%=(long)i+1 %></a></li>
   <%} %>
    <li class="page-item  <%=currentPage + 1 < totalPages ? "" : "disabled" %>"
    <%if(currentPage + 1 < totalPages) { %>
    onclick="load('<%=searchType%>', '<%=sortField %>', '<%=order%>', '<%=(long)((currentPage+1)*pageSize)%>', '<%=length%>', '<%=searchTerm%>')"
    <%} %>
    ><a class="page-link" href="#">Next</a></li>
  </ul>
</nav>
 </div>
</div>
<table class="table">
  <thead>
    <tr>
    <%for(Field f : result.getFields()) { 
     if(f.isHidden()) {
       continue;
     }
    %>
     <th scope="col" style="cursor: pointer;" 
      onclick="load('<%=searchType%>', '<%=f.getName() %>', '<%=f.getName().equals(sortField) && SortOrder.desc == order ? SortOrder.asc : SortOrder.desc%>', '<%=startIndex%>', '<%=length%>', '<%=searchTerm%>')">
      <%=f.getCaption() %>
      <%if(f.getName().equals(sortField)) { 
        if(order == null || order == SortOrder.asc) {
          out.write("<i class='fas fa-sort-alpha-down'></i>");
        } else {
          out.write("<i class='fas fa-sort-alpha-up'></i>");
        }
      } %> 
     </th>
    <%} %>
    <th></th>
    </tr>
  </thead>
  <tbody>
    <%for(List<Object> row : result.getRows()) { 
    boolean isNew = row.get(0).equals(request.getParameter("currencyCode")); 
    %>
    <tr <%if(isNew) { %> style="font-weight: bold; background-color: darkcyan; color: floralwhite;" <%} %>>
     <%for(int i = 0; i < row.size(); i++) {
       if(result.getFields().get(i).isHidden()) {
         continue;
       }
       Object obj = row.get(i);
     %>
      <td scope="col"><%=obj %> <%if(isNew && i == 0) {%><span class="badge badge-primary"><%="insert".equals(mode)? "new":"updated" %></span><%} %></td>
     <%} %>
     <td>
      <button class="btn btn-light" title="Delete" onclick="deleteCurrency('<%=row.get(0) %>', '<%=row.get(1) %>')"><i class="fas fa-trash delete-icon" ></i></button>
      &nbsp; 
      <a href="create.jsp?mode=update&currencyCode=<%=row.get(0) %>" class="btn btn-light" title="Edit"><i class="far fa-edit edit-icon" title="Edit"></i></a>
      
     </td>
    </tr>
    <%} %>
  </tbody>
</table>

<% if(result.getRows().size() == 0) {%>
<div class="alert alert-info" role="alert">
  Now search results
</div>
<% }%>
</div>

<script>
	var currToBeDeleted = "";
	function deleteCurrency(curr, name) {
		currToBeDeleted = curr;
		$(".curr-name").html(name + " (" + curr + ")");
		$('#deleteConfirm').modal();
	}
	
	function confirmDelete() {
		window.location.replace("create.jsp?mode=delete&dbWrite=1&currencyCode=" + currToBeDeleted);
		
	}
	
	function load(searchType, sortField, order, startIndex, length, searchTerm) {
		window.location.replace("search.jsp?" + 
				"searchType=" + searchType + 
				"&sortField=" + sortField + 
				"&order=" + order +
				"&startIndex=" + startIndex + 
				"&length=" + length +
				"&searchTerm=" + searchTerm
				);
	}
</script>

<!-- Modal -->
<div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure you want to delete <strong><span class="curr-name"></span></strong> ? <br> Click Yes to delete, click No to skip.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-danger" onclick="confirmDelete()">Yes</button>
      </div>
    </div>
  </div>
</div>
