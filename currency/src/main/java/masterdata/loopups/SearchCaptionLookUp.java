package masterdata.loopups;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple lookup class.
 * 
 * Currently stores data in an in-memory map. Can easily be changed to read from
 * table.
 * 
 * @author Faraz Ahmed
 *
 */
public class SearchCaptionLookUp {
  private static Map<String, String> captions = new HashMap<>();

  static {
    captions.put("currency", "Search Currency");
  }

  public static String readCaptionForType(String type) {
    return captions.get(type);
  }
}
