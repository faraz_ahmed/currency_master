package masterdata.data;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.base.Strings;

public class DataUpdater {
  public static UpdateResult delete(String tableName, String primaryKey, String primaryKeyValue) {
    UpdateResult result = new UpdateResult();

    DataAccessor.transaction(connection -> {
      String sql = "DELETE FROM " + tableName + " WHERE " + primaryKey + " = '" + primaryKeyValue + "';";
      try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
        preparedStatement.executeUpdate();

      } catch (SQLException e) {
        e.printStackTrace();
      }
    });
    return result;
  }

  public static UpdateResult update(String tableName, String primaryKey, Map<String, String> columnValues) {
    UpdateResult result = new UpdateResult();

    Set<Entry<String, String>> vals = columnValues.entrySet();
    for (Entry<String, String> val : vals) {
      if (Strings.isNullOrEmpty(val.getValue())) {
        result.setSuccessful(false);
        result.setErrorMessage("Column '" + val.getKey() + "' cannot be empty.");
        return result;
      }
    }

    DataAccessor.transaction(connection -> {

      List<String> columns = new ArrayList<>();
      List<String> values = new ArrayList<>();

      for (Entry<String, String> entry : columnValues.entrySet()) {
        columns.add(entry.getKey());
        values.add(entry.getValue());
      }

      String sql = prepareUpdateSql(tableName, primaryKey, columnValues.get(primaryKey), columns);
      try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
        for (int i = 1; i <= values.size(); i++) {
          preparedStatement.setObject(i, values.get(i - 1));
        }
        preparedStatement.executeUpdate();
      } catch (SQLException e) {
        e.printStackTrace();
        result.setSuccessful(false);
        result.setErrorMessage(e.getMessage());
      }

    });

    return result;

  }

  public static UpdateResult insert(String tableName, String primaryKey, Map<String, String> columnValues) {
    UpdateResult result = new UpdateResult();

    Set<Entry<String, String>> vals = columnValues.entrySet();
    for (Entry<String, String> val : vals) {
      if (Strings.isNullOrEmpty(val.getValue())) {
        result.setSuccessful(false);
        result.setErrorMessage("Column '" + val.getKey() + "' cannot be empty.");
        return result;
      }
    }

    DataAccessor.transaction(connection -> {

      // Check if record already exists.
      // Then can be avoided by putting primary key in DB and try to insert
      // If it is duplicate record then DB will throw an error
      try (Statement statement = connection.createStatement();
          ResultSet resultSet = statement.executeQuery("select count(*) from " + tableName + " where " + primaryKey + " = '" + columnValues.get(primaryKey) + "';")) {

        resultSet.next();
        long count = resultSet.getLong(1);
        if (count > 0) {
          result.setSuccessful(false);
          result.setErrorMessage("Record already exists.");
        } else {

          List<String> columns = new ArrayList<>();
          List<String> values = new ArrayList<>();

          for (Entry<String, String> entry : columnValues.entrySet()) {
            columns.add(entry.getKey());
            values.add(entry.getValue());
          }

          String sql = prepareInsertSql(tableName, columns);
          try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
            for (int i = 1; i <= values.size(); i++) {
              preparedStatement.setObject(i, values.get(i - 1));
            }
            preparedStatement.executeUpdate();
          }
        }
      } catch (SQLException e) {
        e.printStackTrace();
        result.setSuccessful(false);
        result.setErrorMessage(e.getMessage());
      }

    });

    return result;
  }

  private static String prepareInsertSql(String tableName, List<String> columns) {
    StringBuilder sb = new StringBuilder();

    sb.append("INSERT INTO ")

        .append(tableName).append(" (")

        .append(columns.stream().collect(Collectors.joining(", ")))

        .append(") VALUES (")

        .append(columns.stream().map(c -> "?").collect(Collectors.joining(", ")))

        .append(" );");

    return sb.toString();
  }

  private static String prepareUpdateSql(String tableName, String primaryKey, String primaryKeyValue, List<String> columns) {
    StringBuilder sb = new StringBuilder();

    sb.append("UPDATE ")

        .append(tableName).append(" SET ")

        .append(columns.stream().collect(Collectors.joining(" = ?, ")))

        .append(" = ?")

        .append(" WHERE ").append(primaryKey).append(" = '").append(primaryKeyValue).append("';");

    return sb.toString();
  }
}
