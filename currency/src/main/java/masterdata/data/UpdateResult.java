package masterdata.data;

public class UpdateResult {
  private boolean successful = true;
  private String errorMessage;

  public UpdateResult(boolean successful, String errorMessage) {
    this.successful = successful;
    this.errorMessage = errorMessage;
  }

  public UpdateResult() {
    super();
  }

  public boolean isSuccessful() {
    return successful;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setSuccessful(boolean successful) {
    this.successful = successful;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

}
