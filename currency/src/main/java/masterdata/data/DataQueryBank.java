package masterdata.data;

import java.util.HashMap;
import java.util.Map;

/**
 * This simple composition can be stored in DB
 *
 */
public class DataQueryBank {
  static Map<String, DataQuery> queries = new HashMap<>();

  static {
    queries.put("currency", new DataQuery("currency", "currencyCode")

        .tables("currency", "country")

        .field("currencyCode", "Code")

        .field("currencyDescription", "Name")

        .field("countryName", "Country")

        .field("currency.countryCode", "Country Code", true)

        .join("currency.countryCode", "country.countryCode")

        .searchFields("currencyCode", "currencyDescription", "countryName"));

    queries.put("country", new DataQuery("country", "countryCode")

        .tables("country")

        .field("countryName", "Country")

        .field("countryCode", "Code"));

  }

}
