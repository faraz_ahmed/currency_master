package masterdata.data;

public class Field {
  private String name;
  private String caption;
  private boolean hidden;

  public Field(String name, String caption) {
    this.name = name;
    this.caption = caption;
  }

  public Field(String name, String caption, boolean hidden) {
    super();
    this.name = name;
    this.caption = caption;
    this.hidden = hidden;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCaption() {
    return caption;
  }

  public void setCaption(String caption) {
    this.caption = caption;
  }

  public boolean isHidden() {
    return hidden;
  }

  public void setHidden(boolean hidden) {
    this.hidden = hidden;
  }

}
