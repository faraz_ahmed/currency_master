package masterdata.data;

import java.util.ArrayList;
import java.util.List;

public class DataResult {

  private List<Field> fields;
  private final List<List<Object>> rows = new ArrayList<>();

  public void setFields(List<Field> fields) {
    this.fields = fields;
  }

  public List<Field> getFields() {
    return fields;
  }

  public List<Object> newRow() {
    List<Object> row = new ArrayList<>();
    rows.add(row);
    return row;
  }

  public List<List<Object>> getRows() {
    return rows;
  }

}
