package masterdata.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.function.Consumer;

public class DataAccessor {
  // Should be externalized in secure config store or in context
  private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
  private static final String MYSQL_URL = "jdbc:mysql://localhost/currencymaster?user=root&password=faraz&useSSL=false";

  static {
    try {
      Class.forName(MYSQL_DRIVER);
    } catch (ClassNotFoundException e1) {
      e1.printStackTrace();
    }
  }

  public static void transaction(Consumer<Connection> code) {
    try (Connection connection = DriverManager.getConnection(MYSQL_URL)) {
      code.accept(connection);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
