package masterdata.data;

public enum SortOrder {
  asc, desc
}
