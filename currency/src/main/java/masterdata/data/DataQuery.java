package masterdata.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.base.Strings;

/**
 * 
 * 
 */
public class DataQuery {
  private final String name;
  private final String primaryKey;
  private final Set<String> tables = new LinkedHashSet<>();
  private final Set<Filter> joins = new LinkedHashSet<>();
  private final List<Field> fields = new ArrayList<>();
  private final Set<String> searchFields = new LinkedHashSet<>();

  public DataQuery(String name, String primaryKey) {
    super();
    this.name = name;
    this.primaryKey = primaryKey;
  }

  public String getName() {
    return name;
  }

  public String getPrimaryKey() {
    return primaryKey;
  }

  /******* Builder Methods **********/

  public DataQuery tables(String... names) {
    tables.addAll(Arrays.asList(names));
    return this;
  }

  public DataQuery join(String from, String to) {
    joins.add(new Filter(from, to));
    return this;
  }

  public DataQuery field(String name, String caption) {
    fields.add(new Field(name, caption));
    return this;
  }

  public DataQuery field(String name, String caption, boolean hidden) {
    fields.add(new Field(name, caption, hidden));
    return this;
  }

  public DataQuery searchFields(String... names) {
    searchFields.addAll(Arrays.asList(names));
    return this;
  }

  /******* End Builder Methods **********/

  public Set<String> getTables() {
    return tables;
  }

  public Set<Filter> getJoins() {
    return joins;
  }

  public List<Field> getFields() {
    return fields;
  }

  public Set<String> getSearchFields() {
    return searchFields;
  }

  /**
   * TODO: Prevent SQL Injection
   * 
   * @param sortField
   * @param order
   * @param startIndex
   * @param length
   * @param searchTerm
   * @return
   */
  public String buildReadSql(String sortField, SortOrder order, int startIndex, int length, String searchTerm, Filter... filters) {
    StringBuilder sb = new StringBuilder();

    sb.append("SELECT ");
    sb.append(fields.stream().map(f -> f.getName()).collect(Collectors.joining(", ")));

    sb.append(" FROM ");
    sb.append(tables.stream().collect(Collectors.joining(", ")));

    boolean whereKeywordAdded = false;
    if (joins.size() > 0) {
      sb.append(" WHERE (");
      whereKeywordAdded = true;

      sb.append(joins.stream().map(j -> j.getField() + " = " + j.getValue()).collect(Collectors.joining(" AND ")));

      sb.append(") ");
    }

    if (!Strings.isNullOrEmpty(searchTerm) && searchFields.size() > 0) {
      if (!whereKeywordAdded) {
        sb.append(" WHERE (");
        whereKeywordAdded = true;
      } else {
        sb.append(" AND (");
      }
      sb.append(searchFields.stream().filter(sf -> !primaryKey.equals(sf)).collect(Collectors.joining(" like '%" + searchTerm + "%' OR ")));
      sb.append(" like '%" + searchTerm + "%' ");

      sb.append("OR ").append(primaryKey).append(" = '").append(searchTerm).append("') ");
    }

    if (filters != null && filters.length > 0) {
      if (!whereKeywordAdded) {
        sb.append(" WHERE (");
        whereKeywordAdded = true;
      } else {
        sb.append(" AND (");
      }

      sb.append(Arrays.asList(filters).stream().map(j -> j.getField() + " = " + j.getValue()).collect(Collectors.joining(" AND ")));

      sb.append(") ");
    }

    if (!Strings.isNullOrEmpty(sortField)) {
      sb.append(" ORDER BY ").append(sortField);

      if (order != null) {
        sb.append(" ").append(order.toString());
      }
    }

    if (startIndex > -1 && length > -1) {
      sb.append(" LIMIT ").append(startIndex).append(",").append(length);
    }

    sb.append(";");

    return sb.toString();
  }

  public String buildCountSql(String searchTerm, Filter... filters) {
    StringBuilder sb = new StringBuilder();

    sb.append("SELECT count(*) FROM ");
    sb.append(tables.stream().collect(Collectors.joining(", ")));

    boolean whereKeywordAdded = false;
    if (joins.size() > 0) {
      sb.append(" WHERE (");
      whereKeywordAdded = true;

      sb.append(joins.stream().map(j -> j.getField() + " = " + j.getValue()).collect(Collectors.joining(" AND ")));

      sb.append(") ");
    }

    if (!Strings.isNullOrEmpty(searchTerm) && searchFields.size() > 0) {
      if (!whereKeywordAdded) {
        sb.append(" WHERE (");
        whereKeywordAdded = true;
      } else {
        sb.append(" AND (");
      }
      sb.append(searchFields.stream().collect(Collectors.joining(" like '%" + searchTerm + "%' OR ")));
      sb.append(" like '%" + searchTerm + "%') ");
    }

    if (filters != null && filters.length > 0) {
      if (!whereKeywordAdded) {
        sb.append(" WHERE (");
        whereKeywordAdded = true;
      } else {
        sb.append(" AND (");
      }

      sb.append(Arrays.asList(filters).stream().map(j -> j.getField() + " = " + j.getValue()).collect(Collectors.joining(" AND ")));

      sb.append(") ");
    }

    sb.append(";");

    return sb.toString();
  }

}
