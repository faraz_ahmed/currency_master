package masterdata.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class DataReader {

  public long count(String queryName, String searchTerm) {

    DataQuery query = DataQueryBank.queries.get(queryName);

    String sql = query.buildCountSql(searchTerm);

    Out<Long> result = new Out<>();

    DataAccessor.transaction(connection -> {
      try (Statement statement = connection.createStatement(); ResultSet resultSet = statement.executeQuery(sql)) {

        resultSet.next();
        result.value = resultSet.getLong(1);

      } catch (SQLException e) {
        e.printStackTrace();
      }
    });

    return result.value;

  }

  public DataResult read(String queryName, String sortField, SortOrder order, int startIndex, int length, String searchTerm, Filter... filters) {

    DataResult result = new DataResult();

    DataQuery query = DataQueryBank.queries.get(queryName);

    result.setFields(query.getFields());

    String sql = query.buildReadSql(sortField, order, startIndex, length, searchTerm, filters);
    DataAccessor.transaction(connection -> {
      try (Statement statement = connection.createStatement(); ResultSet resultSet = statement.executeQuery(sql)) {

        while (resultSet.next()) {
          List<Object> row = result.newRow();
          query.getFields().forEach(f -> {
            try {
              row.add(resultSet.getObject(f.getName()));
            } catch (SQLException e) {
              e.printStackTrace();
            }
          });
        }

      } catch (SQLException e) {
        e.printStackTrace();
      }
    });

    return result;

  }
}
